'use strict';

const fs = require('fs');

module.exports.read = file => {
    return new Promise((done, fail) => {
        fs.readFile(file, 'utf8', (err, data) => {
            if (err) {
                fail(err);
            } else {
                done(data);
            }
        })
    })
};

module.exports.write = (file, data) => {
    return new Promise((done, fail) => {
        fs.writeFile(file, data, err => {
            if (err) {
                fail(err);
            } else {
                done(file);
            }
        })
    })
};