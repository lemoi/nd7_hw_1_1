'use strict';

const fs = require('fs');
const file = require('./file-promise');
const path = require('path');

module.exports = directoryPath => {

    return new Promise((done, fail) => {
        fs.readdir(directoryPath, (err, files) => {
            if (err) {
                fail(err);
            } else {
                done(files);
            }
        })

    }).then(files => {
        let names = files.map(name => path.join(directoryPath, name))
            .filter(name => fs.statSync(name).isFile());

        return Promise.all(names.map(name =>
            file.read(name)
                .then(content => {
                    return {name, content};
                })
        ))

    });

};