'use strict';

const fs = require('fs');

module.exports = (path, callback) => {
    fs.stat(path, (err, stats) => {

        if (err) {
            callback(err);
            return;
        }

        let objFile = {path};
        if (stats.isFile()) {
            objFile.type = 'file';
            //objFile.content = fs.readFileSync(path, 'utf8');
            fs.readFile(path, 'utf8', (err, data) => {
                if (err) throw callback(err);
                objFile.content = data;
                callback(null, objFile);
            });

        } else if (stats.isDirectory()) {
            objFile.type = 'directory';
            //objFile.childs = fs.readdirSync(path);
            fs.readdir(path, (err, files) => {
                if (err) throw callback(err);
                objFile.childs = files;
                callback(null, objFile);
            });

        } else {
            objFile.type = 'undefined';
            callback(null, objFile);
        }

    })
};
