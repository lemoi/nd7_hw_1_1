'use strict';

// Домашнее задание к лекции 1.1 «Модули и работа с файлами»

// Задача 1. Чтение и запись файла на промисах.
console.log('Задача 1. Чтение и запись файла на промисах.');

const file = require('./file-promise');

file
    .read('./data.txt')
    .then(data => data.toUpperCase())
    .then(data => file.write('./upper-data.txt', data))
    .then(filename => console.log(`Создан файл ${filename}`))
    .catch(err => console.error(err));


// Задача 2. Чтение всех файлов в папке на промисах
setTimeout(() => {
    console.log('Задача 2. Чтение всех файлов в папке на промисах');

    const readAll = require('./read-all');

    function show(file) {
        console.log('-'.repeat(10));
        console.log(`Содержимое файла ${file.name}:`);
        console.log(file.content);
        console.log('-'.repeat(10));
    }

    readAll('./logs')
        .then(files => files.forEach(show))
        .catch(err => console.error(err));

}, 2000);

// Задача 3. Информация о файле без промисов
setTimeout(() => {

    console.log('Задача 3. Информация о файле без промисов');

    const pathInfo = require('./path-info');

    function showInfo(err, info) {
        if (err) {
            console.log('Возникла ошибка при получении информации');
            return;
        }

        switch (info.type) {
            case 'file':
                console.log(`${info.path} — является файлом, содержимое:`);
                console.log(info.content);
                console.log('-'.repeat(10));
                break;
            case 'directory':
                console.log(`${info.path} — является папкой, список файлов и папок в ней:`);
                info.childs.forEach(name => console.log(`  ${name}`));
                console.log('-'.repeat(10));
                break;
            default:
                console.log('Данный тип узла не поддерживается');
                break;
        }
    }

    pathInfo(__dirname, showInfo);
    pathInfo(__filename, showInfo);

}, 4000);